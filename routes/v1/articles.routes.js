const express = require("express");
const { index, create, update, destroy, show } = require("../../controllers/v1/article.controller");

const articleRoutes = express.Router();

articleRoutes.get("/", index);
articleRoutes.get("/:id", show);
articleRoutes.post("/", create);
articleRoutes.put("/:id", update);
articleRoutes.patch("/:id", update);
articleRoutes.delete("/:id", destroy);

module.exports = articleRoutes;
