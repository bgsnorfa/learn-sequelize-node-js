const express = require("express");
const { index } = require("../controllers/v1/index.controller");
const articleRoutes = require("./v1/articles.routes");

const router = express.Router();

router.get("/", index);
router.use("/articles", articleRoutes);

module.exports = router;
