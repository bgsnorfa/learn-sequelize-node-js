const { Article } = require("../../models");
const { success, error } = require("../../utils/response/json.utils");

exports.index = async function(req, res) {
    let articles = await Article.findAll({
        attributes: ['id', 'title', 'body', 'approved'],
        order: [
            ['createdAt', 'desc']
        ]
    });

    return success(res, 200, `${articles.length} articles retrieved`, articles);
}

exports.show = async function(req, res) {
    let article = await Article.findByPk(req.params.id);
    if (article === null) {
        return error(res, 404, `Article with id:${req.params.id} not found`);
    } else {
        return success(res, 200, `show article`, {
            id: article.id,
            title: article.title,
            body: article.body,
            approved: article.approved
        });
    }
}

exports.create = async function(req, res) {
    try {
        let article = await Article.create({
            title: req.body.title,
            body: req.body.body,
            approved: req.body.approved
        });

        return success(res, 201, "article created", article);
    } catch (err) {
        return error(res, 422, "validation error", err.errors);
    }
}

exports.update = async function(req, res) {
    try {
        let article = await Article.findOne({
            where: { id: req.params.id }
        });

        if (article) {
            article.title = req.body.title ? req.body.title : article.title;
            article.body = req.body.body ? req.body.body : article.body;
            article.approved = req.body.approved === true ? true : false;

            await article.save();
            return success(res, 200, "article updated", article);
        } else {
            return error(res, 404, "article not found");
        }

    } catch (err) {
        return error(res, 422, "cannot update article", err);
    }
}

exports.destroy = async function(req, res) {
    try {
        const count = await Article.destroy({
            where: { id: req.params.id }
        });

        if (count > 0) {
            return success(res, 200, `${count} article deleted`);
        } else {
            return error(res, 404, "article not found");
        }
    } catch (err) {
        return error(res, 422, "error deleting article", err);
    }
}