const express = require("express");
const apiV1 = {router} = require("./routes/v1.routes");

const bodyParser = require("body-parser");
const jsonParser = bodyParser.json();

const app = express();
const port = process.env.APP_PORT || 3000;

app.use("/api/v1", jsonParser, apiV1);

app.listen(port, () => {
    console.log(`App listening at http://localhost:${port}`);
});
